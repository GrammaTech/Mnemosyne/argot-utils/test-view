(defsystem "test-view"
  :author "GrammaTech"
  :licence "MIT"
  :description "Test structure forms for Argot."
  :long-description "Definitional forms to describe tests and related forms
for use with Argot."
  :depends-on (test-view/test-view)
  :version "0.0.0"
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system))

(register-system-packages "lsp-server" '(:lsp-server/protocol))
(register-system-packages "test-view/test-view" '(:test-view))
