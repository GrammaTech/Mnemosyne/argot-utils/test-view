;;;; General test view code common to all base languages, defined in terms of
;;;the LSP server define-interface macro.

(defpackage :test-view/test-view
  (:use :cl)
  (:import-from :lsp-server/protocol
                :define-interface)
  (:import-from :gt
                :defconst)
  (:import-from :trivial-types
                :proper-list)
  (:export
   :test-case-kind
   :test-case-result
   :unit-test-case-result
   :binary-unit-test-case-result
   :test-case
   :test-case-check-result
   :test-suite))
(in-package :test-view/test-view)

(export (defconst |TestCaseKind.UnitTest| 1))
(export (defconst |TestCaseKind.BinaryTest| 2))

(define-interface |TestCaseResult| ())

(define-interface |UnitTestCaseResult| (|TestCaseResult|)
  (|result| :type string))

(define-interface |BinaryTestCaseResult| (|TestCaseResult|)
   (|stdout| :type string)
   (|stderr| :type string)
   (|exitCode| :type integer))

(define-interface |TestCase| ()
   (|name| :type string)
   (|target| :type string)
   (|testCaseKind| :type integer)
   (|arguments| :type (proper-list string))
   (|expected| :type |TestCaseResult|))

(define-interface |TestCaseCheckResult| ()
   (|testCase| :type |TestCase|)
   (|result| :type |TestCaseResult|)
   (|successful| :type boolean))

(define-interface |TestSuite| ()
  (|testCases| :type (proper-list |TestCase|))
  (|checkResults| :type (proper-list |TestCaseCheckResult|))
  (|environment| :type list))
