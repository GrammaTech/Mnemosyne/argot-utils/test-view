# Test Views

This repository contains Argot server *test view*, which indicates how the
Argot server describes and maintains tests. There are three main components:

**Test Suites**

A test suite has three components:

- `tests`, a list of tests
- `check-results`, a list of any known test check results from checking tests
  (e.g., running the test suite)
- `environment`, an a-list of environment settings

This is the primary way test information will be communicated with Argot. Muses
will receive a JSON-encoded `test-suite` as the right-hand side of the `tests`
entry in the JSON tuple, and will return a (possibly-modified) test suite.

**Tests**

A test has four components:

- `name`, the test's name (should be *unique*)
- `target`, the test's target function, method, binary, etc.
  *(This may need to be extended in the future to handle general regions of
   code, which will involve listing free variables, expected outputs, etc.)*
- `test-case-kind`, indicating the kind of test case (`unit-test`,
  `binary-test`, etc.).
- `arguments`, the test's arguments, which should be passed to the
  target to execute the test.
- `expected`, the test's expected result, as the appropriate
  `test-case-result`.

*Note: in the future, this class will likely need additional values. For*
*example, the following is from the python unittest*
[documentation](https://docs.python.org/3/library/unittest.html#organizing-test-code):

```python
import unittest

class WidgetTestCase(unittest.TestCase):
    def setUp(self):
        self.widget = Widget('The widget')

    def test_default_widget_size(self):
        self.assertEqual(self.widget.size(), (50,50),
                         'incorrect default size')

    def test_widget_resize(self):
        self.widget.resize(100,150)
        self.assertEqual(self.widget.size(), (100,150),
                         'wrong size after resize')
```

The `setUp` and import sections here are relevant: if we want to be able to run
these tests without requiring the entire test file (i.e., as standalone unit
tests at a command line REPL or similar, to provide specific results to an IDE
about a specific function), we will need to support requirements (such as
imports) and pre- and post-setup code. As Argot and its muses mature, we should
revisit tests to expand or subclass it for this purpose.

**Test Case Results**

Test case results represent the expected and actual results of running tests.
The currently come in two flavors:

- `binary-test-case-result`s include `stdout`, `stderr`, and an `exit-code`.
- `unit-test-case-result`s include a string-encoded `result`.

These forms are used for both the expected output associated with a test case
and the actual output from checking a test case.

**Test Case Check Results**

A test case check result has three components:

- `test`, the underlying test it is a result for.
- `result`, the resultant output of running the test (e.g., `stdout` and
  `stderr` for a binary, or the function result as a string for a procedure
  unit test).
- `successful`, a boolean flag indicating if the test produced the expected
  result.

## Outstanding Work

This is a partial list of outstanding work:

- Write test cases to ensure `convert-to-hash-table` and
  `convert-*-from-hash-table` form an isomorphism.
- Define a formal [JSON schema](https://json-schema.org/) for the structures
  in this repository, and keep it in lock-step. (Possibly consider
  auto-generating it.)

## What this Repository is Not

This repository should not include, at any point, any of the following:

- Capabilities to *harvest* test cases or otherwise gather them from a given
  code source or input.
- Capabilities to *execute* test cases, or otherwise check them.

These tasks are should be delegated to *individual muses*, which are capable of
specializing their efforts to an individual language, language form, and even
test case framework. Moreover, it allows muses to arise *naturally* for
specific languages, projects, and frameworks, instead of placing the core
testing functionality under a single umbrella.

## Working with Tests

These definitions are *intentionally spartan*. Any muse that wants to interact
with tests must deal exactly and *only* with these structures. Any internal
structure is left up to the muse, but no amount of that internal structure
should leak beyond the muse; *everything* should be transliterated into these
structures before being returned to Argot.

**To and From JSON**

The three structures above currently support conversion to hash tables, which
may be turned into JSON via `yason:encode`. The converse is also true: any
incoming JSON-formatted set suite should invoke `yason:decode`, then use the
hash table to construct the associated object.

**Extensions**

As discussed in the **Tests** section above, it is possible these JSON forms
will need to be extended to cover additional test cases. *This is okay.* The
goal is *not* to minimize flexibility, but to
*enforce uniformity across clients*. Two different muses that handle a test
case with the same name, target, arguments, and expected result **must**
produce *identical* entries to ensure long-term interoperability across the
entirety of Argot.
